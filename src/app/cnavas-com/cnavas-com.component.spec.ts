import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CnavasComComponent } from './cnavas-com.component';

describe('CnavasComComponent', () => {
  let component: CnavasComComponent;
  let fixture: ComponentFixture<CnavasComComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CnavasComComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CnavasComComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
